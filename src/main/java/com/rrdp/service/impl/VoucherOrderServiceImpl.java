package com.rrdp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rrdp.dto.Result;
import com.rrdp.entity.SeckillVoucher;
import com.rrdp.entity.VoucherOrder;
import com.rrdp.mapper.VoucherOrderMapper;
import com.rrdp.service.ISeckillVoucherService;
import com.rrdp.service.IVoucherOrderService;
import com.rrdp.utils.RedisIdWorker;
import com.rrdp.utils.SimpleRedisLock;
import com.rrdp.utils.UserHolder;
import org.springframework.aop.framework.AopContext;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Coolly
 * @since 2021-12-22
 */
@Service
public class VoucherOrderServiceImpl extends ServiceImpl<VoucherOrderMapper, VoucherOrder> implements IVoucherOrderService {

    @Resource
    private ISeckillVoucherService seckillVoucherService;
    @Resource
    private RedisIdWorker redisIdWorker;
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    @Transactional
    public Result seckillVoucher(Long voucherId) {
        //1、根据优惠券id查询优惠券信息
        SeckillVoucher voucher = seckillVoucherService.getById(voucherId);
        //2、判断秒杀是否开始
        if (voucher.getBeginTime().isAfter(LocalDateTime.now())) {
            return Result.fail("秒杀尚未开始！");
        }
        //3、判断秒杀是否结束
        if (voucher.getEndTime().isBefore(LocalDateTime.now())) {
            return Result.fail("秒杀已结束！");
        }
        //4、判断库存是否充足
        if (voucher.getStock() < 1) {
            return Result.fail("抱歉，秒杀券已被抢购一空！");
        }
        Long userId = UserHolder.getUser().getId();
        SimpleRedisLock lock = new SimpleRedisLock("order:" + userId, stringRedisTemplate);
        boolean isLock = lock.tryLock(1200);
        //判断获取锁是否成功
        if (!isLock) {
            return Result.fail("不允许重复下单！");
        }
        //获取代理对象
        try {
            IVoucherOrderService proxy = (IVoucherOrderService) AopContext.currentProxy();
            return proxy.createVoucherOrder(voucherId);
        } finally {
            //释放锁
            lock.unLock();
        }

    }

    @Transactional
    public Result createVoucherOrder(Long voucherId) {
        //5、一人一单
        Long userId = UserHolder.getUser().getId();
        //5。1、查询订单
        int count = query().eq("user_id", userId).eq("voucher_id", voucherId).count();
        //5.2、判断是否存在
        if (count > 0) {
            return Result.fail("用户已经购买过一次了");
        }

        //6、扣减库存
        boolean success = seckillVoucherService.update()
                .setSql("stock = stock -1")
                .eq("voucher_id", voucherId)
                .gt("stock", 0)
                .update();

        if (!success) {
            return Result.fail("库存不足！");
        }
        //7、创建订单
        VoucherOrder voucherOrder = new VoucherOrder();
        //7.1、设置订单id
        long orderId = redisIdWorker.nextId("order");
        voucherOrder.setId(orderId);
        //7.2、设置用户id
        voucherOrder.setUserId(userId);
        //7.3、设置优惠券id
        voucherOrder.setVoucherId(voucherId);
        save(voucherOrder);
        //返回订单id
        return Result.ok(orderId);
    }
}
