package com.rrdp.service.impl;

import cn.hutool.core.util.BooleanUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.rrdp.dto.Result;
import com.rrdp.entity.Shop;
import com.rrdp.mapper.ShopMapper;
import com.rrdp.service.IShopService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rrdp.utils.CacheClient;
import com.rrdp.utils.RedisData;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import java.time.LocalDateTime;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static com.rrdp.utils.RedisConstants.*;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Coolly
 * @since 2021-12-22
 */
@Service
public class ShopServiceImpl extends ServiceImpl<ShopMapper, Shop> implements IShopService {


    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private CacheClient cacheClient;

    /**
     * 根据店铺id查询
     *
     * @param id
     * @return
     */
    @Override
    public Result queryById(Long id) {
        //缓存穿透
        Shop shop = cacheClient.queryWithPassThrough(CACHE_SHOP_KEY, id, Shop.class, this::getById, CACHE_SHOP_TTL, TimeUnit.SECONDS);
        //互斥锁解决缓存击穿

        //利用逻辑过期解决缓存击穿问题

        if (shop==null) {
            return Result.fail("店铺不存在!");
        }
        return Result.ok(shop);
    }

    private static final ExecutorService CACHE_REBUILD_EXECUTOR = Executors.newFixedThreadPool(10);

    //设置逻辑过期时间设置
    public Shop queryWithLogicalExpire(Long id) {
        String key = CACHE_SHOP_KEY + id;
        //1、从redis查询店铺信息
        String shopCache = stringRedisTemplate.opsForValue().get(key);
        //判断是否为空,为空直接返回
        if (StrUtil.isBlank(shopCache)) {
            //未命中，直接返回
            return null;
        }
        //命中，把json反序列化为对象
        RedisData redisData = JSONUtil.toBean(shopCache, RedisData.class);
        Shop shop = JSONUtil.toBean((JSONObject) redisData.getData(), Shop.class);
        LocalDateTime expireTime = redisData.getExpireTime();
        // 判断缓存是否过期
        if (LocalDateTime.now().isAfter(expireTime)){
            //未过期直接返回信息
            return shop;
        }
        //过期，缓存重建
        //判断是否获取锁成功
        String lockKey = LOCK_SHOP_KEY + id;
        boolean isLock = tryLock(lockKey);
        //成功，返回店铺信息，开启独立线程，实现缓存重建
        if (isLock) {
            CACHE_REBUILD_EXECUTOR.submit(() ->{
                try {
                    //重建缓存
                    this.saveShop2Redis(id,20L);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }finally {
                    //释放锁
                    unlock(lockKey);
                }
            });
        }

        //失败，返回店铺过期信息
        return shop;
    }

    //使用互斥锁解决缓存击穿问题
    public Shop queryWithMutex(Long id) {
        String key = CACHE_SHOP_KEY + id;
        //1、从redis查询店铺信息
        String shopCache = stringRedisTemplate.opsForValue().get(key);
        //判断是否为空,不为空直接返回
        if (StrUtil.isNotBlank(shopCache)) {
            Shop shop = JSONUtil.toBean(shopCache, Shop.class);
            return shop;
        }
        //判断shop是否为空字符串，为空字符串则返回错误信息
        if ("".equals(shopCache)) {
            return null;
        }

        //实现缓存重建
        //1.1获取互斥锁
        String lockKey = "lock:key" + id;
        Shop shop = null;
        try {
            boolean isLock = tryLock(lockKey);
            //1.2是否获取成功
            //1.3失败，休眠并重试
            if (!isLock) {
                Thread.sleep(50);
                return queryWithMutex(id);
            }
            //1.4成功，根据id查询数据库
            //redis里面不存在，从数据库里面查询
            shop = getById(id);
            //不存在，返回错误
            if (shop == null) {
                //避免缓存穿透问题，不存在则返回给redis空字符串
                stringRedisTemplate.opsForValue().set(key, "", CACHE_NULL_TTL, TimeUnit.MINUTES);
                return null;
            }
            //将shop存入redis,设置缓存有效时间30分钟
            stringRedisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(shop), CACHE_SHOP_TTL, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } finally {
            //释放互斥锁
            unlock(lockKey);
        }
        //返回
        return shop;
    }



    private boolean tryLock(String key) {
        //给redis加互斥锁
        Boolean flag = stringRedisTemplate.opsForValue().setIfAbsent(key, "1", 10L, TimeUnit.SECONDS);
        return BooleanUtil.isTrue(flag);
    }

    private void unlock(String key) {
        //解锁
        stringRedisTemplate.delete(key);
    }


    //解决缓存穿透问题
    public Shop queryWithPassThrough(Long id) {
        String key = CACHE_SHOP_KEY + id;
        //1、从redis查询店铺信息
        String shopCache = stringRedisTemplate.opsForValue().get(key);
        //判断是否为空,不为空直接返回
        if (StrUtil.isNotBlank(shopCache)) {
            return JSONUtil.toBean(shopCache, Shop.class);
        }
        //判断shop是否为空字符串，为空字符串则返回错误信息
        if ("".equals(shopCache)) {
            return null;
        }
        //redis里面不存在，从数据库里面查询
        Shop shop = getById(id);
        //不存在，返回错误
        if (shop == null) {
            //避免缓存穿透问题，不存在则返回给redis空字符串
            stringRedisTemplate.opsForValue().set(key, "", CACHE_NULL_TTL, TimeUnit.MINUTES);
            return null;
        }
        //将shop存入redis,设置缓存有效时间30分钟
        stringRedisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(shop), CACHE_SHOP_TTL, TimeUnit.MINUTES);
        //返回
        return shop;
    }




    @Override
    @Transactional
    public Result update(Shop shop) {
        Long id = shop.getId();
        if (id == null) {
            return Result.fail("店铺id不能为空");
        }
        //1、更新数据库
        updateById(shop);
        //2、删除缓存
        stringRedisTemplate.delete(CACHE_SHOP_KEY + id);
        return Result.ok();
    }

    public void saveShop2Redis(Long id,Long expireSeconds) throws InterruptedException {
        //查询店铺信息
        Shop shop = getById(id);
        //模拟延迟
        Thread.sleep(200);
        //封装过期时间
        RedisData redisData = new RedisData();
        redisData.setData(shop);
        redisData.setExpireTime(LocalDateTime.now().plusSeconds(expireSeconds));
        //写入redis
        stringRedisTemplate.opsForValue().set(CACHE_SHOP_KEY + id,JSONUtil.toJsonStr(redisData));
    }
}
