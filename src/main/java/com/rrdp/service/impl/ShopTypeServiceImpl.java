package com.rrdp.service.impl;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rrdp.entity.ShopType;
import com.rrdp.mapper.ShopTypeMapper;
import com.rrdp.service.IShopTypeService;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

import static com.rrdp.utils.RedisConstants.CACHE_SHOP_TYPE_KEY;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Coolly
 * @since 2021-12-22
 */
@Service
public class ShopTypeServiceImpl extends ServiceImpl<ShopTypeMapper, ShopType> implements IShopTypeService {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public List<ShopType> queryList() {
        //从redis缓存里面读取shop-type
        String key = CACHE_SHOP_TYPE_KEY;
        String listCache = stringRedisTemplate.opsForValue().get(key);
        //存在则返回数据
        if (StrUtil.isNotBlank(listCache)) {
            List<ShopType> shopTypes = JSONUtil.toList(listCache, ShopType.class);
            return shopTypes;
        }
        //不存在，则从数据库里面查询
        List<ShopType> shopTypes = query().orderByAsc("sort").list();
        //再将数据库查询到的数据添加到redis中去
        stringRedisTemplate.opsForValue().set(key,JSONUtil.toJsonStr(shopTypes));

        return shopTypes;
    }
}
