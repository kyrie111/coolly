package com.rrdp.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rrdp.dto.LoginFormDTO;
import com.rrdp.dto.Result;
import com.rrdp.dto.UserDTO;
import com.rrdp.entity.User;
import com.rrdp.mapper.UserMapper;
import com.rrdp.service.IUserService;
import com.rrdp.utils.RegexUtils;
import com.rrdp.utils.UserHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static com.rrdp.utils.RedisConstants.*;
import static com.rrdp.utils.SystemConstants.USER_NICK_NAME_PREFIX;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Coolly
 * @since 2021-12-22
 */
@Slf4j
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 发送验证码
     *
     * @param phone
     * @param session
     * @return
     */
    @Override
    public Result sendCode(String phone, HttpSession session) {
        //1、验证手机号格式是否合法
        if (RegexUtils.isPhoneInvalid(phone)) {
            //2、如果不符合，返回错误信息
            return Result.fail("手机号格式错误");
        }
        //3、符合生成验证码(随机生成6位数)
        String code = RandomUtil.randomNumbers(6);
//        //4、保存验证码到session
//        session.setAttribute("code",code);
        //4、保存验证码到redis(设置session有效期，两分钟)
        stringRedisTemplate.opsForValue().set(LOGIN_CODE_KEY + phone, code, LOGIN_CODE_TTL, TimeUnit.MINUTES);
        //5、发送验证码
        log.debug("发送短信验证码成功，验证码：{}", code);
        //返回ok
        return Result.ok();
    }

    /**
     * 短信登录功能
     *
     * @param loginForm
     * @param session
     * @return
     */
    @Override
    public Result login(LoginFormDTO loginForm, HttpSession session) {
        //1、验证手机号格式
        String phone = loginForm.getPhone();
        if (RegexUtils.isPhoneInvalid(phone)) {
            //2、如果不符合，返回错误信息
            return Result.fail("手机号格式错误");
        }
        //3、从redis获取验证码并校验验证码
        String code = stringRedisTemplate.opsForValue().get(LOGIN_CODE_KEY + phone);

        String genCode = loginForm.getCode();
        if (code == null || !genCode.equals(code)) {
            //4、不一致，报错
            return Result.fail("验证码错误");
        }
        //5、一致，根据手机号查询用户
        User user = query().eq("phone", phone).one();
        //判断用户是否为空
        if (user == null) {
            //7、不存在，创建用户
            user = createUserWithPhone(phone);
        }
        //8、保存用户信息到redis去
        //8.1、自定义生成随机token，作为登录令牌,

        String token = UUID.randomUUID().toString();
        //8.2、将user对象转为hash存储
        UserDTO userDTO = BeanUtil.copyProperties(user, UserDTO.class);
        //返回token
        UserHolder.saveUser(userDTO);
        //这里会出现一个因为userDTO里面一个long类型的id，而stringRedisTemplate只支持装string类型，因此需要转型
        Map<String, Object> userMap = BeanUtil.beanToMap(userDTO,new HashMap<>(),
                CopyOptions.create().setIgnoreNullValue(true)
                        .setFieldValueEditor((fileName,fileValue) -> fileValue.toString()));
        //8.3、存储
        String tokenKey = LOGIN_USER_KEY+token;
        stringRedisTemplate.opsForHash().putAll(LOGIN_USER_KEY+token,userMap);

        //设置token过期时间30分钟
        stringRedisTemplate.expire(tokenKey,LOGIN_USER_TTL,TimeUnit.MINUTES);
        UserHolder.saveUser(userDTO);
        return Result.ok(token);
    }

    /**
     * 密码登陆功能
     * @param loginFormDTO
     * @param session
     * @return
     */
    @Override
    public Result loginByPassword(LoginFormDTO loginFormDTO, HttpSession session) {
        //1、验证手机号格式
        String phone = loginFormDTO.getPhone();
        if (RegexUtils.isPhoneInvalid(phone)) {
            //2、如果不符合，返回错误信息
            return Result.fail("手机号格式错误");
        }

        return null;
    }

    /**
     * 登出功能
     * @return
     */
    @Override
    public Result logout() {
        UserHolder.removeUser();
        //通过模糊查询到key，匹配到之后进行删除token
        stringRedisTemplate.keys(LOGIN_USER_KEY + "*");
        return Result.ok("登出成功");
    }

    private User createUserWithPhone(String phone) {
        //1、创建用户
        User user = new User();
        user.setPhone(phone);
        user.setPassword("");
        user.setNickName(USER_NICK_NAME_PREFIX + RandomUtil.randomString(10));
        //2、保存用户
        save(user);
        return user;
    }
}
