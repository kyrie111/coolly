package com.rrdp.service;

import com.rrdp.dto.Result;
import com.rrdp.entity.Blog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Coolly
 * @since 2021-12-22
 */
public interface IBlogService extends IService<Blog> {

    Result queryBlog(Long id);

    Result queryHotBlog(Integer current);

    Result likeBlog(Long id);

    Result queryTop5(Long id);

    Result saveBlog(Blog blog);

    Result queryBlogOfFollow(Long max, Integer offset);
}
