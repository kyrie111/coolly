package com.rrdp.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author Cooly
 * @date 2022/10/23
 */
public interface FileService {
    //文件上传的方法
    String upload(MultipartFile file);
}
