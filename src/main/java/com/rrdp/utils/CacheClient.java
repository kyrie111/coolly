package com.rrdp.utils;

import cn.hutool.core.util.BooleanUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import static com.rrdp.utils.RedisConstants.*;

/**
 * @author Cooly
 * @date 2022/10/15
 */
@Slf4j
@Component
public class CacheClient {

    //避免无效的修改
    private final StringRedisTemplate stringRedisTemplate;

    public CacheClient(StringRedisTemplate stringRedisTemplate) {
        this.stringRedisTemplate = stringRedisTemplate;
    }

    public void setStringRedisTemplate(String key, Object value, Long time, TimeUnit unit) {
        stringRedisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(value), time, unit);
    }


    public void setWithLogic(String key, Object value, Long time, TimeUnit unit) {
        //设置逻辑过期
        RedisData redisData = new RedisData();
        redisData.setData(value);
        redisData.setExpireTime(LocalDateTime.now().plusSeconds(unit.toSeconds(time)));
        //写入redis
        stringRedisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(redisData));
    }


    //解决缓存穿透问题
    public <R, ID> R queryWithPassThrough(String keyPrefix, ID id, Class<R> type, Function<ID, R> dbFallback, Long time, TimeUnit unit) {
        String key = keyPrefix + id;
        //1、从redis查询店铺信息
        String json = stringRedisTemplate.opsForValue().get(key);
        //判断是否为空,不为空直接返回
        if (StrUtil.isNotBlank(json)) {
            return JSONUtil.toBean(json, type);
        }
        //判断shop是否为空字符串，为空字符串则返回错误信息
        if ("".equals(json)) {
            return null;
        }
        //redis里面不存在，从数据库里面查询
        R r = dbFallback.apply(id);
        //不存在，返回错误
        if (r == null) {
            //避免缓存穿透问题，不存在则返回给redis空字符串
            stringRedisTemplate.opsForValue().set(key, "", CACHE_NULL_TTL, TimeUnit.MINUTES);
            return null;
        }
        //将shop存入redis,设置缓存有效时间30分钟
        this.setStringRedisTemplate(key, r, time, unit);
        //返回
        return r;
    }




    private boolean tryLock(String key) {
        //给redis加互斥锁
        Boolean flag = stringRedisTemplate.opsForValue().setIfAbsent(key, "1", 10, TimeUnit.SECONDS);
        return BooleanUtil.isTrue(flag);
    }

    private void unlock(String key) {
        //解锁
        stringRedisTemplate.delete(key);
    }

    //使用互斥锁解决缓存击穿问题
    public <R,ID> R queryWithMutex(String keyPrefix,ID id,Class<R> type,Function<ID,R> dbFallback) {
        String key = keyPrefix + id;
        //1、从redis查询店铺信息
        String json = stringRedisTemplate.opsForValue().get(key);
        //判断是否为空,不为空直接返回
        if (StrUtil.isNotBlank(json)) {
            R r = JSONUtil.toBean(json, type);
            return r;
        }
        //判断shop是否为空字符串，为空字符串则返回错误信息
        if ("".equals(json)) {
            return null;
        }
        //实现缓存重建
        //1.1获取互斥锁
        String lockKey = "lock:key" + id;
        R r = null;
        try {
            boolean isLock = tryLock(lockKey);
            //1.2是否获取成功
            //1.3失败，休眠并重试
            if (!isLock) {
                Thread.sleep(50);
                return queryWithMutex(keyPrefix,id,type,dbFallback);
            }
            //1.4成功，根据id查询数据库
            //redis里面不存在，从数据库里面查询
            r = dbFallback.apply(id);
            //不存在，返回错误
            if (r == null) {
                //避免缓存穿透问题，不存在则返回给redis空字符串
                stringRedisTemplate.opsForValue().set(key, "", CACHE_NULL_TTL, TimeUnit.MINUTES);
                return null;
            }
            //将shop存入redis,设置缓存有效时间30分钟
            stringRedisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(r), CACHE_SHOP_TTL, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } finally {
            //释放互斥锁
            unlock(lockKey);
        }
        //返回
        return r;

    }


}
