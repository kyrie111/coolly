package com.rrdp.utils;

import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;

import java.util.Collections;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author Cooly
 * @date 2022/10/18
 */
public class SimpleRedisLock implements ILock {

    private String name;
    private StringRedisTemplate stringRedisTemplate;

    public SimpleRedisLock(String name, StringRedisTemplate stringRedisTemplate) {
        this.name = name;
        this.stringRedisTemplate = stringRedisTemplate;
    }

    private static final String KEY_PREFIX = "lock:";
    private static final String ID_PREFIX = UUID.randomUUID().toString();
    private static final DefaultRedisScript<Long> UNLOCK_SCRIPT;

    static {
        UNLOCK_SCRIPT = new DefaultRedisScript<>();
        UNLOCK_SCRIPT.setLocation(new ClassPathResource("unlock.lua"));
        UNLOCK_SCRIPT.setResultType(Long.class);
    }

    @Override
    public boolean tryLock(long timeoutSec) {
        //获取锁
        String threatId = ID_PREFIX + Thread.currentThread().getId();
        Boolean success = stringRedisTemplate.opsForValue()
                .setIfAbsent(KEY_PREFIX + name, threatId, timeoutSec, TimeUnit.SECONDS);
        return Boolean.TRUE.equals(success);
    }


    //使用lua脚本去释放锁
    @Override
    public void unLock() {
        stringRedisTemplate
                .execute(UNLOCK_SCRIPT,
                        Collections.singletonList(KEY_PREFIX + name),
                        ID_PREFIX + Thread.currentThread().getId());
    }

    /**
     @Override public void unLock() {
     //获取当前线程锁线程ID
     String threatId = ID_PREFIX + Thread.currentThread().getId();
     //获取redis里面的锁的值
     String redisLock = stringRedisTemplate.opsForValue().get(KEY_PREFIX + name);
     //如果两个一致说明为同一个线程的操作
     if (threatId.equals(redisLock)) {
     //释放锁
     stringRedisTemplate.delete(KEY_PREFIX + name);
     }
     }*/
}
