package com.rrdp.utils;

/**
 *
 * @author Cooly
 * @date 2022/10/18
 */
public interface ILock {

    /**
     * 尝试获取锁
     * @param timeoutSec 锁持有的超时时间
     * @return  true代表获取锁成功
     */
    boolean tryLock(long timeoutSec);

    /**
     * 释放锁
     */
    void unLock();
}
