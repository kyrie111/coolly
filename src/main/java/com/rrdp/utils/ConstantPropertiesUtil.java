package com.rrdp.utils;


import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author Cooly
 * @date 2022/10/1
 */
@Component
public class ConstantPropertiesUtil implements InitializingBean {


    private String region = "ap-shanghai";

    private String secretId = "AKIDpW0KH7AMRPESUSGwRvVzIRdieYL4hDZq";


    private String secretKey = "eDmEYjqz0riTvlV7YotwnR25DH6vAZvG";


    private String bucketName = "peoples-comments-1314188876";

    public static String END_POINT;
    public static String ACCESS_KEY_ID;
    public static String ACCESS_KEY_SECRET;
    public static String BUCKET_NAME;

    @Override
    public void afterPropertiesSet() throws Exception {
        END_POINT = region;
        ACCESS_KEY_ID = secretId;
        ACCESS_KEY_SECRET = secretKey;
        BUCKET_NAME = bucketName;
    }
}
